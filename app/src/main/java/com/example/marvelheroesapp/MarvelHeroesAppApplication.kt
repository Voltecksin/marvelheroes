package com.example.marvelheroesapp

import android.app.Application
import com.example.marvelheroesapp.timber.ReleaseTree
import com.facebook.stetho.Stetho
import com.squareup.leakcanary.LeakCanary
import io.realm.Realm
import timber.log.Timber

class MarvelHeroesAppApplication : Application() {

    //Initiate everything here
    override fun onCreate() {
        super.onCreate()
        initializeTimber()
        initializeStetho()
        initializeLeakCanary()
        initializeRealm()
    }

    private fun initializeTimber() {
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree()) else Timber.plant(ReleaseTree())
    }

    private fun initializeStetho() {
        Stetho.initializeWithDefaults(this)
    }

    private fun initializeLeakCanary() {
        if (LeakCanary.isInAnalyzerProcess(this)) return
        LeakCanary.install(this)
    }

    private fun initializeRealm() = Realm.init(this)
}