package com.example.marvelheroesapp.repository.model.remote

import com.google.gson.annotations.SerializedName

data class SuperHeroes(
    @SerializedName("superheroes")
    var superHeroes: List<SuperHero>? = emptyList()
)