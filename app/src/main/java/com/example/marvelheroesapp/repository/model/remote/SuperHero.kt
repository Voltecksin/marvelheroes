package com.example.marvelheroesapp.repository.model.remote

import com.google.gson.annotations.SerializedName

data class SuperHero(

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("photo")
    var photo: String? = null,

    @SerializedName("realName")
    var realName: String? = null,

    @SerializedName("height")
    var height: String? = null,

    @SerializedName("power")
    var power: String? = null,

    @SerializedName("abilities")
    var abilities: String? = null,

    @SerializedName("groups")
    var groups: String? = null
)