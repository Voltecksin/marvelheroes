package com.example.marvelheroesapp.repository.model.local.dao

import com.example.marvelheroesapp.repository.model.local.SuperHero
import com.example.marvelheroesapp.repository.model.local.SuperHeroes
import com.example.marvelheroesapp.utils.realm.LiveRealmData
import com.example.marvelheroesapp.utils.realm.asLiveData
import io.realm.Realm
import io.realm.RealmList
import java.util.*

class SuperHeroesDao(private val realm: Realm) {

    fun addSuperHeroes(superHeroesList: RealmList<SuperHero>) {
        realm.executeTransaction { realm.insertOrUpdate(SuperHeroes(UUID.randomUUID().toString(),superHeroesList)) }
    }

    fun findAllHeroes(): LiveRealmData<SuperHeroes> =
        (realm.where(SuperHeroes::class.java).findAllAsync()).asLiveData()
}