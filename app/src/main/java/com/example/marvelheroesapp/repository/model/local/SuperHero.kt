package com.example.marvelheroesapp.repository.model.local

import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*

@RealmClass
open class SuperHero(

    @PrimaryKey
    var pk: String = UUID.randomUUID().toString(),

    var name: String? = null,

    var photo: String? = null,

    var realName: String? = null,

    var height: String? = null,

    var power: String? = null,

    var abilities: String? = null,

    var groups: String? = null
) : RealmModel