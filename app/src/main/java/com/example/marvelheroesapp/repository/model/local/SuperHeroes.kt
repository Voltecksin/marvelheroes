package com.example.marvelheroesapp.repository.model.local

import io.realm.RealmList
import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*

@RealmClass
open class SuperHeroes(

    @PrimaryKey
    var pk: String = UUID.randomUUID().toString(),

    var superHeroes: RealmList<SuperHero>? = null
) : RealmModel