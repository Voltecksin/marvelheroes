package com.example.marvelheroesapp.timber

import android.util.Log
import timber.log.Timber

class ReleaseTree : Timber.Tree() {

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if(priority == Log.ERROR || priority == Log.WARN){
            //Do nothing
        }
    }
}