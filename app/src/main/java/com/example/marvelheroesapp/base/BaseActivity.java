package com.example.marvelheroesapp.base;

import android.os.Bundle;
import androidx.annotation.CallSuper;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.ViewModelProviders;
import org.jetbrains.annotations.NotNull;

public abstract class BaseActivity<V extends BaseContract.View, P extends BaseContract.Presenter<V>>
        extends AppCompatActivity implements BaseContract.View {
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    protected P presenter;

    private Toolbar toolbar = null;

    @SuppressWarnings("unchecked")
    @CallSuper
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BaseViewModel<V, P> viewModel = ViewModelProviders.of(this).get(BaseViewModel.class);

        if (viewModel.getPresenter() == null) {
            viewModel.setPresenter(initPresenter());
        }
        presenter = viewModel.getPresenter();
        presenter.attachLifecycle(getLifecycle());
        presenter.attachView((V) this);
    }

    @Override
    @NotNull
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }

    @CallSuper
    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachLifecycle(getLifecycle());
        presenter.detachView();
    }

    protected abstract P initPresenter();

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStackImmediate();
            return;
        }
        super.onBackPressed();
    }

    @Nullable
    protected Toolbar getToolbar() {
        return toolbar;
    }

    protected void setupToolbar(@NotNull Toolbar toolbar, @NotNull String toolbarTitle,
                                boolean homeButtonEnabled) {
        this.toolbar = toolbar;

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(homeButtonEnabled);
            getSupportActionBar().setDisplayHomeAsUpEnabled(homeButtonEnabled);
            getSupportActionBar().setDisplayShowHomeEnabled(homeButtonEnabled);
            getSupportActionBar().setTitle(toolbarTitle);
        }
    }

    protected void changeToolbarTitle(@NotNull String toolbarTitle) {
        if (getSupportActionBar() != null) getSupportActionBar().setTitle(toolbarTitle);
    }
}
