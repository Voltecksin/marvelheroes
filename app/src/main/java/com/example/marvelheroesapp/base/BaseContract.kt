package com.example.marvelheroesapp.base

import androidx.lifecycle.Lifecycle

interface BaseContract {

    interface View

    interface Presenter<V : View> {

        fun getView(): V?

        fun getLifecycleCurrentState(): Lifecycle.State

        fun isViewAttached(): Boolean

        fun attachLifecycle(lifecycle: Lifecycle)

        fun detachLifecycle(lifecycle: Lifecycle)

        fun attachView(view: V)

        fun detachView()
    }
}