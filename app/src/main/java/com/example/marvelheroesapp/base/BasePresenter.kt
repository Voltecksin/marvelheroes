package com.example.marvelheroesapp.base

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver

abstract class BasePresenter<V : BaseContract.View> : LifecycleObserver, BaseContract.Presenter<V> {

    private var view: V? = null
    private lateinit var lifecycle: Lifecycle

    override fun getView(): V? = view

    override fun attachLifecycle(lifecycle: Lifecycle) {
        lifecycle.addObserver(this)
        this.lifecycle = lifecycle
    }

    override fun detachLifecycle(lifecycle: Lifecycle) {
        lifecycle.removeObserver(this)
    }

    override fun attachView(view: V) {
        this.view = view
    }

    override fun detachView() {
        view = null
    }

    override fun isViewAttached(): Boolean = view != null

    /**
     * Method used to get the lifecycle inside the presenter,
     * must be used just to get the lifecycleState, with: getCurrentState
     */
    @Throws(UninitializedPropertyAccessException::class)
    override fun getLifecycleCurrentState(): Lifecycle.State = lifecycle.currentState
}