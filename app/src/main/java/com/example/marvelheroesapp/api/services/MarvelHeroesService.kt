package com.example.marvelheroesapp.api.services

import com.example.marvelheroesapp.api.ApiEndpoint
import com.example.marvelheroesapp.repository.model.remote.SuperHeroes
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

interface MarvelHeroesService {

    @GET(ApiEndpoint.MARVEL_HEROES)
    fun getMarvelHeroes(): Observable<Response<SuperHeroes>>
}