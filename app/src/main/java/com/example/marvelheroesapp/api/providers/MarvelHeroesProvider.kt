package com.example.marvelheroesapp.api.providers

import com.example.marvelheroesapp.api.ApiHelper
import com.example.marvelheroesapp.api.services.MarvelHeroesService
import com.example.marvelheroesapp.repository.model.remote.SuperHeroes
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class MarvelHeroesProvider {

    private val marvelHeroesService: MarvelHeroesService by lazy {
        ApiHelper.createRetrofitMarvelHeroes().create(MarvelHeroesService::class.java)
    }

    fun getMarvelHeroes(): Observable<Response<SuperHeroes>> =
        marvelHeroesService.getMarvelHeroes()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
}