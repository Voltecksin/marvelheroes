package com.example.marvelheroesapp.utils.realm

import com.example.marvelheroesapp.repository.model.local.dao.SuperHeroesDao
import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmResults

fun Realm.superHeroesModel(): SuperHeroesDao = SuperHeroesDao(this)

fun <T : RealmModel> RealmResults<T>.asLiveData() = LiveRealmData(this)